# ratmom

this is ratmom, a [libcurl]-based http client for rust that makes sense.

_a fork of [isahc]._

[![Crates.io](https://img.shields.io/crates/v/ratmom.svg)](https://crates.io/crates/ratmom)
[![Documentation](https://docs.rs/ratmom/badge.svg)][documentation]
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![Crates.io downloads](https://img.shields.io/crates/d/ratmom)](https://crates.io/crates/ratmom)
![Maintenance](https://img.shields.io/badge/maintenance-actively--developed-brightgreen.svg)
[![Build](https://ci.codeberg.org/api/badges/transcast/ratmom/status.svg)](https://ci.codeberg.org/transcast/ratmom)

## differences from isahc (TODO)

-   no more synchronous API
-   fuck [http] crate
-   [url] crate used for url like everywhere else
-   [ratcurl], our fork of the [curl crate] used
-   curl-impersonate support

## unchanged key features

-   full support for HTTP/1.1 and HTTP/2.
-   configurable request timeouts, redirect policies, Unix sockets, and many more settings.
-   a runtime-agnostic asynchronous API with support for [async/await].
-   fully asynchronous core, with incremental reading and writing of request and response bodies and connection multiplexing.
-   sessions and cookie persistence.
-   automatic request cancellation on drop.

## [documentation]

please check out the [documentation] for details on what ratmom can do and how to use it. to get you started, here is a really simple, complete example that spits out the response body from https://example.org:

```rust
use ratmom::prelude::*;

fn main() -> Result<(), ratmom::Error> {
    // Send a GET request and wait for the response headers.
    // Must be `mut` so we can read the response body.
    let mut response = ratmom::get("https://example.org")?;

    // Print some basic info about the response to standard output.
    println!("Status: {}", response.status());
    println!("Headers: {:#?}", response.headers());

    // Read the response body as text into a string and print it.
    print!("{}", response.text()?);

    Ok(())
}
```

click [here][documentation] for documentation on the latest version.

## getting help

need some help with something ratmom-related? ask a question on our [issues page][issues], where we are happy to try and answer your questions!

## installation

install via cargo by adding to your `Cargo.toml` file:

```toml
[dependencies]
ratmom = "0.1"
```

or simply:

```
cargo add ratmom
```

## license

this project's source code and documentation are licensed under the MIT license. see the [LICENSE](LICENSE) file for details.

[async/await]: https://rust-lang.github.io/async-book/01_getting_started/04_async_await_primer.html
[curl crate]: https://crates.io/crates/curl
[documentation]: https://docs.rs/ratmom
[http]: https://github.com/hyperium/http
[isahc]: https://github.com/sagebind/isahc
[issues]: https://codeberg.org/transcast/ratmom/issues
[libcurl]: https://curl.haxx.se/libcurl/
[ratcurl]: https://codeberg.org/transcast/ratcurl
[url]: https://crates.io/crates/url
