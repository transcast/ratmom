use std::io::Cursor;

use ratmom::{Body, Request};

fn main() -> Result<(), ratmom::Error> {
    let weird_request =
        Request::head("http://download.opensuse.org/update/tumbleweed/repodata/repomd.xml")
            .body(Body::from_reader(Cursor::new(b"")))?;

    let error = ratmom::send(weird_request).unwrap_err();
    eprintln!("{}", error);

    Ok(())
}
