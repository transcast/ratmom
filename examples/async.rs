//! This example demonstrates the use of `send_async()` to make a request
//! asynchronously using the futures-based API.

use futures_lite::future::block_on;
use ratmom::prelude::*;

fn main() -> Result<(), ratmom::Error> {
    block_on(async {
        let mut response = ratmom::get_async("http://example.org").await?;

        println!("Status: {}", response.status());
        println!("Headers:\n{:?}", response.headers());
        println!("Body: {}", response.text().await?);

        Ok(())
    })
}
